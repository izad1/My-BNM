//
//  CurrenciesTableViewController.h
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurrenciesTableViewDelegate.h"
#import "FavoriteButton.h"
#import "Key.h"

@interface CurrenciesTableViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *rate;
@property (nonatomic) BOOL inverted;
@property (nonatomic, strong) Key *key;
@property (nonatomic, assign) id <CurrenciesTableViewDelegate> delegate;

- (IBAction)inverseButtonTapped:(UIBarButtonItem *)sender;
- (IBAction)sortButtonTapped:(UIBarButtonItem *)sender;
- (void)favoriteButtonTapped:(FavoriteButton *)sender;

@end
