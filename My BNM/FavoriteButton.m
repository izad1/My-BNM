//
//  FavoriteButton.m
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "FavoriteButton.h"

@implementation FavoriteButton

- (void)setFavorited:(BOOL)favorited {
    _favorited = favorited;
    
    if (_favorited) {
        [self setImage:[UIImage imageNamed:@"FavFilled"] forState:UIControlStateNormal];
    }
    else {
        [self setImage:[UIImage imageNamed:@"Fav"] forState:UIControlStateNormal];
    }
}



@end
