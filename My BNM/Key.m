//
//  Key.m
//  My BNM
//
//  Created by Izad Che Muda on 16/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "Key.h"
#import "Favorite.h"

@implementation Key

NSString * const kKeyName = @"keys";


- (id)initWithRate:(NSDictionary *)rate {
    self = [super init];
    
    if (self) {
        self.rate = rate;
    }
    
    return self;
}


- (NSArray *)orderedKeys {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kKeyName]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:kKeyName];
    }
    else {
        return [[self.rate objectForKey:@"meta"] objectForKey:@"orderedKeys"];
    }
}


- (void)setOrderedKeys:(NSArray *)orderedKeys {
    [[NSUserDefaults standardUserDefaults] setObject:orderedKeys forKey:kKeyName];
}


- (NSArray *)favoritedKeys {
    NSArray *keys;
    NSArray *favorites = [Favorite favorites];
    NSArray *savedKeys = [[NSUserDefaults standardUserDefaults] objectForKey:kKeyName];
    
    if (savedKeys) {
        if (favorites) {
            NSMutableArray *favoritedKeys = [NSMutableArray new];
            
            [savedKeys enumerateObjectsUsingBlock:^(NSString *currencyCode, NSUInteger idx, BOOL *stop) {
                if ([favorites containsObject:currencyCode]) {
                    [favoritedKeys addObject:currencyCode];
                }
            }];
            
            keys = favoritedKeys;
        }
        else {
            keys = savedKeys;
        }
    }
    else {
        keys = [[self.rate objectForKey:@"meta"] objectForKey:@"orderedKeys"];
    }
    
    return keys;
}

@end
