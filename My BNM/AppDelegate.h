//
//  AppDelegate.h
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

