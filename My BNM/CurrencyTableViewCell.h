//
//  CurrencyTableViewCell.h
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavoriteButton.h"

@interface CurrencyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet FavoriteButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *coversionLabel;

- (void)configureWithCurrencyCode:(NSString *)currencyCode data:(NSDictionary *)data inverted:(BOOL)inverted favorites:(NSArray *)favorites;

@end
