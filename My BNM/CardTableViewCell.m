//
//  CardTableViewCell.m
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "CardTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "Flag.h"
#import "Favorite.h"
#import "Key.h"

@implementation CardTableViewCell

- (void)awakeFromNib {
    // Initialization code
}


- (void)configureWithRate:(NSDictionary *)rate {
    NSDictionary *meta = [rate objectForKey:@"meta"];
    
    NSNumber *timestamp = [meta objectForKey:@"lastUpdateTimestamp"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp.doubleValue];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM d"];
    
    self.dateLabel.text = [dateFormat stringFromDate:date];
    
    [self.flagImageViews enumerateObjectsUsingBlock:^(UIImageView *flagImageView, NSUInteger idx, BOOL *stop) {
        flagImageView.hidden = YES;
    }];
    
    [self.currencyLabels enumerateObjectsUsingBlock:^(UILabel *currencyLabel, NSUInteger idx, BOOL *stop) {
        currencyLabel.hidden = YES;
    }];
    
    [self.rateLabels enumerateObjectsUsingBlock:^(UILabel *rateLabel, NSUInteger idx, BOOL *stop) {
        rateLabel.hidden = YES;
    }];
    
    Key *key = [[Key alloc] initWithRate:rate];
    NSArray *keys = key.favoritedKeys;
    NSArray *favorites = [Favorite favorites];
    
    NSInteger length = favorites ? favorites.count : 6;
    
    for (int i = 0; i < length; i++) {
        NSString *currencyCode = [keys objectAtIndex:i];
        NSDictionary *data = [rate objectForKey:@"data"];
        
        UILabel *currencyLabel = [self.currencyLabels objectAtIndex:i];
        currencyLabel.text = currencyCode;
        currencyLabel.hidden = NO;
        
        UILabel *rateLabel = [self.rateLabels objectAtIndex:i];
        rateLabel.text = [[data objectForKey:currencyCode] objectForKey:@"rateValue"];
        rateLabel.hidden = NO;
        
        UIImageView *flagImageView = [self.flagImageViews objectAtIndex:i];
        [flagImageView setImageWithURL:[Flag URLFromCurrencyCode:currencyCode]];
        flagImageView.hidden = NO;
    }
}

@end
