//
//  MainTableViewController.m
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "MainTableViewController.h"
#import "CardTableViewCell.h"
#import "FooterTableViewCell.h"
#import "AFNetworking.h"


@implementation MainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    [self fetchRateWithNetworkActivityIndicatorVisible:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.rate) {
        return 1;
    }
    
    return 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        CardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"card" forIndexPath:indexPath];
        [cell configureWithRate:self.rate];
        
        return cell;
    }
    else if (indexPath.row == 1) {
        FooterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"footer" forIndexPath:indexPath];
        
        return cell;
    }
    
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            return 245;
            
        case 1:
            return 50;
            
        default:
            return 0;
    }
}



#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"showCurrencies" sender:nil];
    }
}



#pragma mark - Misc.


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCurrencies"]) {
        CurrenciesTableViewController *currenciesTVC = segue.destinationViewController;
        currenciesTVC.rate = self.rate;
        currenciesTVC.delegate = self;
    }
}


- (void)didUpdateData {
    [self.tableView reloadData];
}


- (void)refresh {
    [self fetchRateWithNetworkActivityIndicatorVisible:NO];
}

- (void)fetchRateWithNetworkActivityIndicatorVisible:(BOOL)visible {
    if (visible) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    
    [manager GET:@"http://misc.mobeegen.com/bnm/cms/api/getFxRate"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, NSDictionary *rate) {
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             [self.refreshControl endRefreshing];
             self.rate = rate;
             
             [self.tableView reloadData];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             [self.refreshControl endRefreshing];
             NSLog(@"%@", error);
         }];
}

@end
