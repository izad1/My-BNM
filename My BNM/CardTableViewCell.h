//
//  CardTableViewCell.h
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *flagImageViews;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *currencyLabels;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *rateLabels;

- (void)configureWithRate:(NSDictionary *)rate;

@end
