//
//  Flag.m
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "Flag.h"

@implementation Flag

+ (NSURL *)URLFromCurrencyCode:(NSString *)currencyCode {
    if ([currencyCode isEqualToString:@"EUR"]) {
        return [NSURL URLWithString:@"http://www.wpclipart.com/flags/eu__flag_of_europe.png"];
    }
    
    if ([currencyCode isEqualToString:@"HKD"]) {
        return [NSURL URLWithString:@"http://www.flagsinformation.com/hong_kong-flag.png"];
    }
    
    if ([currencyCode isEqualToString:@"SDR"]) {
        return [NSURL URLWithString:@"http://cdn.static-economist.com/sites/default/files/imagecache/original-size/images/print-edition/20140329_LDC999.png"];
    }
         
    NSDictionary *countryCodes = @{
      @"AED": @"ae",
      @"AUD": @"au",
      @"BND": @"bn",
      @"CAD": @"ca",
      @"CHF": @"fr",
      @"CNY": @"cn",
      @"EGP": @"eg",
      @"GBP": @"gb",
      @"IDR": @"id",
      @"INR": @"in",
      @"JPY": @"jp",
      @"KHR": @"kh",
      @"KRW": @"kr",
      @"MMK": @"mm",
      @"NPR": @"np",
      @"NZD": @"nz",
      @"PHP": @"ph",
      @"PKR": @"pk",
      @"SAR": @"sa",
      @"SGD": @"sg",
      @"THB": @"th",
      @"TWD": @"tw",
      @"USD": @"us",
      @"VND": @"vn"
    };
    
    NSString *string = [NSString stringWithFormat:@"http://flagpedia.net/data/flags/normal/%@.png", [countryCodes objectForKey:currencyCode]];

    return [NSURL URLWithString:string];
}

@end
