//
//  CurrenciesTableViewController.m
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "CurrenciesTableViewController.h"
#import "CurrencyTableViewCell.h"
#import "Favorite.h"


@implementation CurrenciesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.inverted = NO;
    self.key = [[Key alloc] initWithRate:self.rate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *data = [self.rate objectForKey:@"data"];

    return data.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *currencyCode = [self.key.orderedKeys objectAtIndex:indexPath.row];
    NSDictionary *data = [self.rate objectForKey:@"data"];
    
    
    CurrencyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"currency" forIndexPath:indexPath];
    [cell.favoriteButton addTarget:self action:@selector(favoriteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell configureWithCurrencyCode:currencyCode
                               data:[data objectForKey:currencyCode]
                           inverted:self.inverted
                          favorites:[Favorite favorites]];
    
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}


- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSMutableArray *keys = [NSMutableArray arrayWithArray:self.key.orderedKeys];
    
    NSString *currencyCode = [keys objectAtIndex:sourceIndexPath.row];    
    [keys removeObjectAtIndex:sourceIndexPath.row];
    [keys insertObject:currencyCode atIndex:destinationIndexPath.row];
    
    self.key.orderedKeys = keys;
    [self.delegate didUpdateData];
}


#pragma mark - Misc.


- (IBAction)inverseButtonTapped:(UIBarButtonItem *)sender {
    self.inverted = !self.inverted;
    [self.tableView reloadData];
}

- (IBAction)sortButtonTapped:(UIBarButtonItem *)sender {
    if (self.tableView.editing) {
        [self.tableView setEditing:NO animated:YES];
        sender.image = [UIImage imageNamed:@"Sort"];
    }
    else {
        [self.tableView setEditing:YES animated:YES];
        sender.image = [UIImage imageNamed:@"Done"];
    }
}


- (NSArray *)keys {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"keys"]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:@"keys"];
    }
    else {
        return [[self.rate objectForKey:@"meta"] objectForKey:@"orderedKeys"];
    }
}


- (void)setKeys:(NSArray *)keys {
    [[NSUserDefaults standardUserDefaults] setObject:keys forKey:@"keys"];
    [self.delegate didUpdateData];
}


- (void)favoriteButtonTapped:(FavoriteButton *)sender {
    NSArray *favorites = [Favorite favorites];
    
    if (favorites && favorites.count == 6 && sender.favorited == NO) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                       message:@"You are only allowed a maximum of 6 favorite currencies."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        sender.favorited = !sender.favorited;
        
        if (sender.favorited) {
            [Favorite addCurrencyCode:sender.currencyCode];
            
            NSMutableArray *keys = [NSMutableArray arrayWithArray:self.key.orderedKeys];
            NSInteger index = [keys indexOfObject:sender.currencyCode];
            [keys removeObjectAtIndex:index];
            [keys insertObject:sender.currencyCode atIndex:[Favorite favorites].count - 1];
            
            self.key.orderedKeys = keys;
            [self.delegate didUpdateData];
            [self.tableView reloadData];
        }
        else {
            [Favorite removeCurrencyCode:sender.currencyCode];
        }
        
        [self.delegate didUpdateData];
    }
}

@end
