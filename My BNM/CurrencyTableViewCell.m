//
//  CurrencyTableViewCell.m
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "CurrencyTableViewCell.h"
#import "Flag.h"
#import "UIImageView+AFNetworking.h"


@implementation CurrencyTableViewCell

- (void)awakeFromNib {
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


- (void)configureWithCurrencyCode:(NSString *)currencyCode data:(NSDictionary *)data inverted:(BOOL)inverted favorites:(NSArray *)favorites {
    self.favoriteButton.currencyCode = currencyCode;
    
    self.flagImageView.image = nil;
    [self.flagImageView setImageWithURL:[Flag URLFromCurrencyCode:currencyCode]];
    self.currencyLabel.text = currencyCode;
    
    if (inverted) {
        self.coversionLabel.text = [data objectForKey:@"invRateLabel"];
    }
    else {
        self.coversionLabel.text = [data objectForKey:@"rateLabel"];
    }
    
    self.favoriteButton.favorited = [favorites containsObject:currencyCode];
}

@end
