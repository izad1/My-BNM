//
//  FooterTableViewCell.m
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "FooterTableViewCell.h"
#import "HexColors.h"

@implementation FooterTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if (selected) {
        self.containerView.backgroundColor = [UIColor colorWithHexString:@"#f8f8f8"];
    }
    else {
        self.containerView.backgroundColor = [UIColor whiteColor];
    }
}

@end
