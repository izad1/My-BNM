//
//  CurrenciesTableViewDelegate.h
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

@protocol CurrenciesTableViewDelegate <NSObject>

- (void)didUpdateData;

@end