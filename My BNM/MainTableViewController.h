//
//  MainTableViewController.h
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurrenciesTableViewController.h"

@interface MainTableViewController : UITableViewController <CurrenciesTableViewDelegate>

@property (nonatomic, strong) NSDictionary *rate;

- (void)fetchRateWithNetworkActivityIndicatorVisible:(BOOL)visible;
- (void)refresh;

@end
