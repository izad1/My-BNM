//
//  Favorite.h
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Favorite : NSObject

+ (NSArray *)favorites;
+ (void)addCurrencyCode:(NSString *)currencyCode;
+ (void)removeCurrencyCode:(NSString *)currencyCode;

@end
