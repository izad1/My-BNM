//
//  Favorite.m
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "Favorite.h"

@implementation Favorite

NSString * const kFavoritesKeyName = @"favorites";

+ (NSArray *)favorites {
    return [[NSUserDefaults standardUserDefaults] objectForKey:kFavoritesKeyName];
}


+ (void)addCurrencyCode:(NSString *)currencyCode {
    NSMutableArray *favorites = [NSMutableArray arrayWithArray:[self favorites]];
    [favorites addObject:currencyCode];
    
    NSInteger start = favorites.count - 6;
    
    if (start < 0) {
        start = 0;
    }
    
    NSInteger length;
    
    if (favorites.count < 6) {
        length = favorites.count;
    }
    else {
        length = 6;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[favorites subarrayWithRange:NSMakeRange(start, length)] forKey:kFavoritesKeyName];
}


+ (void)removeCurrencyCode:(NSString *)currencyCode {
    NSMutableArray *favorites = [NSMutableArray arrayWithArray:[self favorites]];
    [favorites removeObject:currencyCode];
    
    if (favorites.count) {
        [[NSUserDefaults standardUserDefaults] setObject:favorites forKey:kFavoritesKeyName];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kFavoritesKeyName];
    }
}

@end
