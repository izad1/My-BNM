//
//  FavoriteButton.h
//  My BNM
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteButton : UIButton

@property (nonatomic) BOOL favorited;
@property (nonatomic, strong) NSString *currencyCode;

@end
