//
//  Key.h
//  My BNM
//
//  Created by Izad Che Muda on 16/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Key : NSObject

- (id)initWithRate:(NSDictionary *)rate;

@property (nonatomic, strong) NSDictionary *rate;
@property (nonatomic, strong) NSArray *orderedKeys;
@property (nonatomic, strong, readonly) NSArray *favoritedKeys;

@end
