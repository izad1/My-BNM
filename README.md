# About

![My BNM Screenshots](http://i.imgur.com/qUuWXnj.png)

This app is developed as a part of Websight Sdn Bhd's technical test.

# Running the App

* Open `My BNM.xcworkspace` and hit `⌘R` to run
* It is advisable to update the app's dependencies by running `pod install` first
